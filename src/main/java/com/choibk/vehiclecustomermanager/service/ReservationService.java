package com.choibk.vehiclecustomermanager.service;

import com.choibk.vehiclecustomermanager.entity.Reservation;
import com.choibk.vehiclecustomermanager.model.ListResult;
import com.choibk.vehiclecustomermanager.model.ReservationItem;
import com.choibk.vehiclecustomermanager.model.ReservationRequest;
import com.choibk.vehiclecustomermanager.repository.ReservationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ReservationService {
    private final ReservationRepository reservationRepository;

    public void setReservation(ReservationRequest request) {
        Reservation addData = new Reservation.ReservationBuilder(request).build();

        reservationRepository.save(addData);
    }

    public ListResult<ReservationItem> getReservations() {
        List<Reservation> originList = reservationRepository.findAll();

        List<ReservationItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new ReservationItem.ReservationItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
}
