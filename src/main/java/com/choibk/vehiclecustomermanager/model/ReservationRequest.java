package com.choibk.vehiclecustomermanager.model;

import com.choibk.vehiclecustomermanager.enums.RentType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class ReservationRequest {
    @ApiModelProperty(value = "대여 타입", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private RentType rentType;

    @ApiModelProperty(value = "고객명", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(value = "고객 전화번호", required = true)
    @NotNull
    @Length(min = 11, max = 20)
    private String phoneNumber;

    @ApiModelProperty(value = "상담 요청을 받은 시간", required = true)
    @NotNull
    private LocalDateTime counselingRequestTime;
}
