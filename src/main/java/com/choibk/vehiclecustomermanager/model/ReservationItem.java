package com.choibk.vehiclecustomermanager.model;

import com.choibk.vehiclecustomermanager.Interfaces.CommonModelBuilder;
import com.choibk.vehiclecustomermanager.entity.Reservation;
import com.choibk.vehiclecustomermanager.enums.RentType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReservationItem {
    private Long id;
    private RentType rentType;
    private String name;
    private String phoneNumber;
    private LocalDateTime counselingRequestTime;

    private ReservationItem(ReservationItemBuilder builder) {
        this.id = builder.id;
        this.rentType = builder.rentType;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.counselingRequestTime = builder.counselingRequestTime;
    }

    public static class ReservationItemBuilder implements CommonModelBuilder<ReservationItem> {
        private final Long id;
        private final RentType rentType;
        private final String name;
        private final String phoneNumber;
        private final LocalDateTime counselingRequestTime;

        public ReservationItemBuilder(Reservation reservation) {
            this.id = reservation.getId();
            this.rentType = reservation.getRentType();
            this.name = reservation.getName();
            this.phoneNumber = reservation.getPhoneNumber();
            this.counselingRequestTime = reservation.getCounselingRequestTime();
        }

        @Override
        public ReservationItem build() {
            return new ReservationItem(this);
        }
    }

}
