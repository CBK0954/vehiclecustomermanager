package com.choibk.vehiclecustomermanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleCustomerManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(VehicleCustomerManagerApplication.class, args);
    }

}
