package com.choibk.vehiclecustomermanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RentType {
    RENT("렌드"),
    LEASE("리스"),
    UNDETERMINED("미정");

    private final String borrowType;
}
