package com.choibk.vehiclecustomermanager.repository;

import com.choibk.vehiclecustomermanager.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
}
