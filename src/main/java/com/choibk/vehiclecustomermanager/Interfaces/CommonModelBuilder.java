package com.choibk.vehiclecustomermanager.Interfaces;

public interface CommonModelBuilder<T> {
    T build();
}