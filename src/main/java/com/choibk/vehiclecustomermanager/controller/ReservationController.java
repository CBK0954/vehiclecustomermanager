package com.choibk.vehiclecustomermanager.controller;

import com.choibk.vehiclecustomermanager.model.CommonResult;
import com.choibk.vehiclecustomermanager.model.ListResult;
import com.choibk.vehiclecustomermanager.model.ReservationItem;
import com.choibk.vehiclecustomermanager.model.ReservationRequest;
import com.choibk.vehiclecustomermanager.service.ReservationService;
import com.choibk.vehiclecustomermanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "고객 상담 내역")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/reservation")
public class ReservationController {
    private final ReservationService reservationService;

    @ApiOperation(value = "고객 정보 입력")
    @PostMapping("/data")
    public CommonResult setReservation(@RequestBody @Valid ReservationRequest request) {
        reservationService.setReservation(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "고객 정보 리스트")
    @GetMapping("/all")
    public ListResult<ReservationItem> getReservations() {
        return ResponseService.getListResult(reservationService.getReservations(), true);
    }
}