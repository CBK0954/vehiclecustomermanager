package com.choibk.vehiclecustomermanager.entity;

import com.choibk.vehiclecustomermanager.Interfaces.CommonModelBuilder;
import com.choibk.vehiclecustomermanager.enums.RentType;
import com.choibk.vehiclecustomermanager.model.ReservationRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private RentType rentType;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 20)
    private String phoneNumber;

    @Column(nullable = false)
    private LocalDateTime counselingRequestTime;

    private Reservation(ReservationBuilder builder) {
        this.rentType = builder.rentType;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.counselingRequestTime = builder.counselingRequestTime;
    }

    public static class ReservationBuilder implements CommonModelBuilder<Reservation> {
        private final RentType rentType;
        private final String name;
        private final String phoneNumber;
        private final LocalDateTime counselingRequestTime;

        public ReservationBuilder(ReservationRequest request) {
            this.rentType = request.getRentType();
            this.name = request.getName();
            this.phoneNumber = request.getPhoneNumber();
            this.counselingRequestTime = request.getCounselingRequestTime();
        }
        @Override
        public Reservation build() {
            return new Reservation(this);
        }
    }
}
